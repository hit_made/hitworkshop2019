
"""Touch Demo."""
from time import sleep
from machine import Pin, PWM, TouchPad

touch = {
    0: TouchPad(Pin(4)),
    1: TouchPad(Pin(2)),
    2: TouchPad(Pin(15)),
    3: TouchPad(Pin(32)),  # TouchPad pin 32 and 33 reversed (fixed?)
    4: TouchPad(Pin(27)),
    5: TouchPad(Pin(14)),
    6: TouchPad(Pin(12)),
    7: TouchPad(Pin(13))}

threshold = {
    0: [],
    1: [],
    2: [],
    3: [],
    4: [],
    5: [],
    6: [],
    7: []}

print('Calibrating...')
# Scan each TouchPad 12 times
for x in range(12):
    for i in range(8):
        threshold[i].append(touch[i].read())
    sleep(.1)
# Store average threshold values
for i in range(8):
    print('Range{0}: {1}-{2}'.format(i, min(threshold[i]), max(threshold[i])))
    threshold[i] = sum(threshold[i]) // len(threshold[i])
    print('Threshold{0}: {1}'.format(i, threshold[i]))

try:
    while True:
        press = False
        for i in range(8):
            capacitance = touch[i].read()
            cap_ratio = capacitance / threshold[i]
            # Check if current TouchPad is pressed
            if i < 3 and .5 < cap_ratio < .93:
                press = True

                # Execute selected command
                sleep(.1)  # Debounce button press
                print('Pressed {0}: {1}, Diff: {2}, Ratio: {3}%.'.format(
                      i, capacitance,
                      threshold[i] - capacitance, cap_ratio * 100))
            elif i >= 3 and .5 < cap_ratio < .95:
                press = True
                print('Pressed {0}: {1}, Diff: {2}, Ratio: {3}%.'.format(
                      i, capacitance,
                      threshold[i] - capacitance, cap_ratio * 100))
        if press is False:
          print("No Press")
        sleep(.1)

except KeyboardInterrupt:
    print('\nCtrl-C pressed.  Cleaning up and exiting...')


