from machine import I2C, Pin
import mpu6050
import time
i2c = I2C(scl=Pin(5), sda=Pin(4))
accelerometer = mpu6050.accel(i2c)
while True:
  print(accelerometer.get_values())
  time.sleep(0.1)

