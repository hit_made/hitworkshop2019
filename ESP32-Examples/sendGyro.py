import urequests
import ujson
import network
import time
import machine
from bno055 import *

try:
  import usocket as socket
except:
  import socket

SSID="RobotinoNetwork"
PASSWORD="RobotinoPassword"
RASPI_IP="192.168.1.123"
wlan=None

i2c = machine.I2C(-1, scl=machine.Pin(5), sda=machine.Pin(4), timeout=1000)
imu = BNO055(i2c)
calibrated = False


def connectWifi(ssid,passwd):
  global wlan
  wlan=network.WLAN(network.STA_IF)         #create a wlan object
  wlan.active(True)                         #Activate the network interface
  wlan.disconnect()                         #Disconnect the last connected WiFi
  wlan.connect(ssid,passwd)                 #connect wifi
  print("try to connect\n")
  while(wlan.ifconfig()[0]=='0.0.0.0'):
    time.sleep(1)
  return True


try:
  connectWifi(SSID,PASSWORD)
  while True:
    time.sleep(0.1)
    if not calibrated:
        calibrated = imu.calibrated()
        print('Calibration required: sys {} gyro {} accel {} mag {}'.format(*imu.cal_status()))
    
    dict = {}
    dict["SensorID"] = "Gyro"
    dict["SensorValue"] =  str(imu.gyro())
    sensordata=ujson.dumps(dict)
    # Post (HTTP/Post) the touch data to the Raspi Webserver
   
    time.sleep(0.5)
    if not calibrated:
        calibrated = imu.calibrated()
        print('Calibration required: sys {} gyro {} accel {} mag {}'.format(*imu.cal_status()))
    
    dict = {}
    dict["SensorID"] = "Gyro"
    dict["SensorValue"] =  str('x {:5.0f}    y {:5.0f}     z {:5.0f}'.format(*imu.gyro()))
    sensordata=ujson.dumps(dict)
    print(sensordata)
    # Post (HTTP/Post) the touch data to the Raspi Webserver
    response = urequests.post("http://"+RASPI_IP+"/esp", headers={"Content-Type": "application/json"}, data = sensordata)

except KeyboardInterrupt:
    print('\nCtrl-C pressed.  Cleaning up and exiting...')
