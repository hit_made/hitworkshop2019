import time
from umqttsimple import MQTTClient
import ubinascii
import machine
import micropython
import network
import mpu6050
i2c = machine.I2C(scl=machine.Pin(5), sda=machine.Pin(4))
accelerometer = mpu6050.accel(i2c)
import gc
gc.collect()

ssid = 'MainRaspberry'
password = 'made_lab'
mqtt_server = '192.168.4.1'

client_id = ubinascii.hexlify(machine.unique_id())
topic_pub = b'notification'

station = network.WLAN(network.STA_IF)

station.active(True)
station.connect(ssid, password)

while station.isconnected() == False:
  pass

print('Connection successful')
print(station.ifconfig())

def sub_cb(topic, msg):
  print((topic, msg))
  if topic == b'notification' and msg == b'received':
    print('ESP received hello message')

def connect_and_subscribe():
  global client_id, mqtt_server, topic_sub
  client = MQTTClient(client_id, mqtt_server)
  client.set_callback(sub_cb)
  client.connect()
  #client.subscribe(topic_sub)
  #print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_sub))
  return client

def restart_and_reconnect():
  print('Failed to connect to MQTT broker. Reconnecting...')
  time.sleep(10)
  machine.reset()
  
try:
  client = connect_and_subscribe()
except OSError as e:
  restart_and_reconnect()

mid=(int(station.ifconfig()[0].split(".")[-1]))
while True:
  try:   
    msg = b'ID: %d GyX: %d GyY: %d GyZ: %d AcX: %d AcY %d AcZ: %d'%(mid, accelerometer.get_values()['GyX'],accelerometer.get_values()['GyY'],accelerometer.get_values()['GyZ'],accelerometer.get_values()['AcX'],accelerometer.get_values()['AcY'],accelerometer.get_values()['AcZ'])
    client.publish(topic_pub, msg)
    time.sleep(0.1)
  except OSError as e:
    restart_and_reconnect()

