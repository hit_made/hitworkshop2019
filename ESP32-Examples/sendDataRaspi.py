from machine import Pin, PWM, TouchPad
import urequests
import ujson
import network
import time

SSID="RobotinoNetwork"
PASSWORD="RobotinoPassword"
RASPI_IP="192.168.1.123"
wlan=None


touch = {
    0: TouchPad(Pin(4)),
    1: TouchPad(Pin(2)),
    2: TouchPad(Pin(15)),
    3: TouchPad(Pin(32)),  
    4: TouchPad(Pin(27)),
    5: TouchPad(Pin(14)),
    6: TouchPad(Pin(12)),
    7: TouchPad(Pin(13))}

threshold = {
    0: [],
    1: [],
    2: [],
    3: [],
    4: [],
    5: [],
    6: [],
    7: []}

print('Calibrating...')
# Scan each TouchPad 12 times
for x in range(12):
    for i in range(8):
        threshold[i].append(touch[i].read())
    time.sleep(.1)
# Store average threshold values
for i in range(8):
    print('Range{0}: {1}-{2}\n'.format(i, min(threshold[i]), max(threshold[i])))
    threshold[i] = sum(threshold[i]) // len(threshold[i])
    print('Threshold{0}: {1}\n'.format(i, threshold[i]))

def connectWifi(ssid,passwd):
  global wlan
  wlan=network.WLAN(network.STA_IF)         #create a wlan object
  wlan.active(True)                         #Activate the network interface
  wlan.disconnect()                         #Disconnect the last connected WiFi
  wlan.connect(ssid,passwd)                 #connect wifi
  print("try to connect\n")
  while(wlan.ifconfig()[0]=='0.0.0.0'):
    time.sleep(1)
  return True

#Catch exceptions,stop program if interrupted accidentally in the 'try'
try:
  connectWifi(SSID,PASSWORD)
  while True:
    press = False
    for i in range(8):
      capacitance = touch[i].read()
      cap_ratio = capacitance / threshold[i]
      # Check if current TouchPad is pressed
      if .5 < cap_ratio < .95:
        press = True
        # Execute selected command
        time.sleep(.1)  # Debounce button press
        print('Pressed {0}: {1}, Diff: {2}, Ratio: {3}%.'.format(
          i, capacitance,
          threshold[i] - capacitance, cap_ratio * 100))
      if press is False:
        time.sleep(.1)
      else:
        # Create JSON Formated Touch Information
        dict = {}
        dict["SensorID"] = str(i)
        dict["SensorValue"] =  str(capacitance)
        sensordata=ujson.dumps(dict)
        print("Sending Data:"+sensordata)
        # Post (HTTP/Post) the touch data to the Raspi Webserver
        response = urequests.post("http://"+RASPI_IP+"/esp", headers={"Content-Type": "application/json"}, data = sensordata)
        print(response.json())
        #time.sleep(.1)

except KeyboardInterrupt:
    print('\nCtrl-C pressed.  Cleaning up and exiting...')




