# Make music with a micro:bit (by P Dring, Fulford School, York)

import speech
import random
from microbit import *

# lines of the song to sing in a random order
lines = ["Happy hacking everyone", "Enjoy your class", "Coding is fun"]

# mouth images
mouth_closed = Image("09090:00000:90009:09990:00000")
mouth_open   = Image("09090:00000:90909:09090:00900")

# keep looping
while True:
  # choose a random line and and split string into words
  text = random.choice(lines)
  words = text.split(" ")

  # sing each word at a different pitch
  for word in words:
    display.show(mouth_closed)
    sleep(20)
    
    # wait for button A to be pressed
    while not button_a.is_pressed():
      sleep(50)
    
    # convert word to phoneme so we can sing it
    phoneme = speech.translate(word)
    
    # we want a pitch between 20 and 115
    # see http://microbit-micropython.readthedocs.io/en/latest/tutorials/speech.html
    pitch = accelerometer.get_x() # number from -1024 to 1024
    pitch += 1024.0               # translate it to a number between 0 and 2048
    pitch *= (95.0 / 2048.0)      # scale it to a number between 0 and 95
    pitch = int(115 - pitch)      # translate it into a number between 115 (low) and 20 (high)
   
    # we want a speed between 0 (fast) and 255 (slow)
    s = accelerometer.get_y()     # number between -1024 and 1024
    s += 1024.0                   # translate it to a number between 0 and 2048
    s *= (255.0 / 2048.0)         # scale it to a number between 0 and 255
    s = int(255.0 - s)            # change it to a number from 255 (slow) to 0 (fast)
 
    display.show(mouth_open)
    speech.sing("#" + str(pitch) + phoneme, speed=s)
