#!/usr/bin/env python3
# https://create.withcode.uk/

import RPi.GPIO as GPIO
import time

PIN1=14
# Setup Pins
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN1, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

try:
  while True:
    if GPIO.input(PIN1) == GPIO.HIGH:
        print "Too close"
    else: 
        print "Go Ahead"
except:
  GPIO.output(PIN1, GPIO.LOW)
  GPIO.cleanup()
