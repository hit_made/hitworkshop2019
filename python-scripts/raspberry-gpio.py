#!/usr/bin/env python3
# https://create.withcode.uk/

import RPi.GPIO as GPIO
import time

# Setup Pins
GPIO.setmode(GPIO.BOARD)
GPIO.setup(3, GPIO.OUT)
GPIO.setup(5, GPIO.IN)

# Loop 10 times
for i in range(10):
    
    # Write Pin 3
    GPIO.output(3, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(3, GPIO.LOW)
    time.sleep(0.5)
    
    # Read Input Pin 5
    if GPIO.input(5) == GPIO.HIGH:
        print("Pin 5 is on")
    else:
        print("Pin 5 is off")
