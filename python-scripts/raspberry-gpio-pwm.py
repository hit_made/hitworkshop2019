import RPi.GPIO as GPIO
import time

servoPIN = 17
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN, GPIO.OUT)

# GPIO 17 for PWM with 50Hz
motor = GPIO.PWM(servoPIN, 50) 

motor.start(2.5) # Initialization

for dc in range(0, 101, 5):
  motor.ChangeDutyCycle(dc)
  print (dc)
  time.sleep(0.1)
for dc in range(100, -1, -5):
  motor.ChangeDutyCycle(dc)
  print(dc)
  time.sleep(0.1)

motor.stop()
GPIO.cleanup()
