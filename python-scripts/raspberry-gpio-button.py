#!/usr/bin/env python3
# https://create.withcode.uk/

import RPi.GPIO as GPIO
import time

PIN1=24
PIN2=23
# Setup Pins
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN1, GPIO.OUT)
GPIO.setup(PIN2, GPIO.IN, pull_up_down=GPIO.PUD_UP)



try:
  while True:
    if GPIO.input(PIN2) == GPIO.LOW:
        GPIO.output(PIN1, GPIO.HIGH)
    else: 
        GPIO.output(PIN1, GPIO.LOW)
except:
  GPIO.output(PIN1, GPIO.LOW)
  GPIO.cleanup()
