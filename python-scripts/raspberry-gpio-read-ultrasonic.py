#!/usr/bin/env python3
# Example Ultrasonic Distance Sensor

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

# Imports and initial mode

TRIG = 23
ECHO = 24

GPIO.setup(TRIG,GPIO.OUT)
GPIO.setup(ECHO,GPIO.IN)


# Issue Trigger Signal
GPIO.output(TRIG, True)
time.sleep(0.00001)
GPIO.output(TRIG, False)

while GPIO.input(ECHO) == False:
    start = time.time()

while GPIO.input(ECHO) == True:
    end = time.time()

# print(str(signal_time) + "=" + str(end) +  "-"  + str(start))
signal_time = end-start

# Calculate Distance in CM:
distance = signal_time / 0.000058

print('Distance: {} centimeters'.format(distance))

GPIO.cleanup()


