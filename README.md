## Content
- Introduction to Rapid  Prototyping using Physical Computing (21/05/2019)
- RaspberryPI and ESP32 hands on with (Micro)Python (22/05/2019)

## Picture References (in pdfs)
- Images displaying code are screenshots generated from https://create.withcode.uk/ 
- Circuits drawn with fritzing.org
- Physics images: https://www.grund-wissen.de/physik Bernhard Grotz (Creative Commons License, Version 3.0, by-nc-sa)  
