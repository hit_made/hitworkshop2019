## MQTT on a Raspberry PI:
(steps from https://randomnerdtutorials.com/~n-raspberry-pi/)

### Installing:
```
sudo apt install -y mosquitto mosquitto-clients
sudo systemctl enable mosquitto.service
mosquitto -v
```

### Start daemon
```
mosquitto -d
```
### Test a topic
```
mosquitto_sub -d -t testTopic
mosquitto_pub ...
```

