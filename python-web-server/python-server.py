from flask import Flask, abort, request, render_template
import json

app = Flask(__name__)
recData = []

@app.route('/esp', methods=['POST']) 
def esp():
	global recData
	if not request.json:
		abort(400)
	print request.json
	recData.append(request.json)
	return json.dumps(request.json)
    
    
@app.route('/', methods=['GET']) 
def display_data():
	global recData
	print recData
	return render_template('index.html', data=recData)
	


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
